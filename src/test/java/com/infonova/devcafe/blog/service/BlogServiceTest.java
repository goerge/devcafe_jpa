package com.infonova.devcafe.blog.service;

import com.infonova.devcafe.blog.AbstractTest;
import com.infonova.devcafe.blog.model.Blog;
import com.infonova.devcafe.blog.model.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional
public class BlogServiceTest extends AbstractTest {

  private static final String USERNAME = "blogUser";
  @Autowired
  private BlogService blogService;

  @Before
  public void prepareUserData() {
    em.persist(createBlogUser());
  }

  private User createBlogUser() {
    User blogUser = new User();
    blogUser.setUsername(USERNAME);
    blogUser.setFirstName("firstName");
    blogUser.seteMail("mail@me.com");
    blogUser.setLastName("lastName");
    return blogUser;
  }

  @Test
  public void addBlogEntry() {
    String blogEntry = "This is my first Entry";
    Blog blog = createBlog(blogEntry);

    blogService.addBlogEntry(USERNAME, blog);
    assertNotNull(blog.getEntryTime());
    assertNotNull(blog.getId());
    assertNotNull(em.find(Blog.class, blog.getId()));
  }

  @Test
  public void addBlogEntries() {
    Blog blog = createBlog("First entry");
    Blog blog2 = createBlog("Second entry");
    Blog blog3 = createBlog("Third entry");

    blogService.addBlogEntry(USERNAME, blog);
    blogService.addBlogEntry(USERNAME, blog2);
    blogService.addBlogEntry(USERNAME, blog3);

    em.clear();
    User user = em.find(User.class, USERNAME);
    assertEquals(3, user.getBlogEntries().size());
  }

  private Blog createBlog(String blogEntry) {
    Blog blog = new Blog();
    blog.setBlogEntry(blogEntry);
    return blog;
  }
}