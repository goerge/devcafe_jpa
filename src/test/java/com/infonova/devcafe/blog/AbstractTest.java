package com.infonova.devcafe.blog;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public abstract class AbstractTest {

	@PersistenceContext
	protected EntityManager em;

//	@Before
//	public void clearDB() {
//		em.getTransaction().begin();
//		em.createQuery("Delete from Comment c").executeUpdate();
//		em.createQuery("Delete from Blog b").executeUpdate();
//		em.createQuery("Delete from User u").executeUpdate();
//		em.createQuery("Delete from Address a").executeUpdate();
//		em.getTransaction().commit();
//	}

}