package com.infonova.devcafe.blog.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.persistence.OptimisticLockException;

import com.infonova.devcafe.blog.AbstractTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.infonova.devcafe.blog.model.Blog;
import com.infonova.devcafe.blog.model.PremiumBlog;
import com.infonova.devcafe.blog.model.User;

@Transactional
public class BlogDaoTest extends AbstractTest {

	private static final String USERNAME = "username";

	@Autowired
	private BlogDao blogDao;

	@Autowired
	private UserDao userDao;

	@Before
	public void prepareUserData() {
		userDao.saveEntity(createBlogUser());
	}

	@Test
	public void saveBlogEntry() {
		Blog blog = new PremiumBlog();
		blog.setBlogEntry("This is an awesome entry");
		blog.setEntryTitle("Watch out!");
		blog.setUser(new User(USERNAME));
		((PremiumBlog) blog).setVisits(100);
		blogDao.saveEntity(blog);

		assertNotNull(blog.getId());
		em.clear();
		assertTrue(em.find(Blog.class, blog.getId()) instanceof PremiumBlog);
	}

	@Test
	public void updateBlogEntry() {
		Blog blog = new Blog();
		blog.setBlogEntry("This is an awesome entry");
		blog.setEntryTitle("Watch out!");
		blog.setUser(new User(USERNAME));
		Blog updatedEntity = blogDao.updateEntity(blog);

		assertNull(blog.getId());
		assertNotSame(blog, updatedEntity);
	}

	@Test
	public void updateBlogEntryValues() {
		Blog saveBlog = new Blog();
		saveBlog.setBlogEntry("This is an awesome entry");
		saveBlog.setEntryTitle("Watch out!");
		saveBlog.setUser(new User(USERNAME));
		blogDao.saveEntity(saveBlog);
		Long persistedBlogId = saveBlog.getId();

		Blog blog = blogDao.findEntity(persistedBlogId);
		blog.setEntryTitle("new awesome title");
		blog.setBlogEntry("another blog Entry");
		blogDao.updateEntity(blog);

		Blog dbBlogEntity = blogDao.findEntity(persistedBlogId);
		assertEquals("new awesome title", dbBlogEntity.getEntryTitle());
		assertEquals("another blog Entry", dbBlogEntity.getBlogEntry());
	}

	@Test(expected = OptimisticLockException.class)
	public void optimisticLockingTest() throws InterruptedException {
		Blog blog = new Blog();
		blog.setBlogEntry("This is an awesome entry");
		blog.setEntryTitle("Watch out!");
		blog.setUser(new User(USERNAME));
		blogDao.saveEntity(blog);
		em.flush();
		em.clear();

		Long blogId = blog.getId();

		Blog blog2 = blogDao.findEntity(blogId);
		blog2.setEntryTitle("new title of blog 2");
		blogDao.updateEntity(blog2);
		em.flush();
		em.clear();

		blog.setEntryTitle("new title of blog 1");
		blogDao.updateEntity(blog);
	}

	private User createBlogUser() {
		User blogUser = new User();
		blogUser.setUsername(USERNAME);
		blogUser.setFirstName("firstName");
		blogUser.seteMail("mail@me.com");
		blogUser.setLastName("lastName");
		return blogUser;
	}
}