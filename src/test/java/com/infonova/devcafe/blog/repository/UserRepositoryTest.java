package com.infonova.devcafe.blog.repository;

import com.infonova.devcafe.blog.AbstractTest;
import com.infonova.devcafe.blog.model.Address;
import com.infonova.devcafe.blog.model.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Iterator;
import java.util.List;

import static com.infonova.devcafe.blog.repository.UserSpecs.livesInSlovakia;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static com.infonova.devcafe.blog.repository.UserSpecs.*;
import static org.springframework.data.jpa.domain.Specifications.where;

@Transactional
public class UserRepositoryTest extends AbstractTest {

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setUpTestData() {
    userRepository.save(createUser("lubos.bistak", "Lubos", "Bistak", "lubos@bistak.sk", createAddress("Ulica 12", "Bratislava", "85101")));
    userRepository.save(createUser("thomas.offner", "Thomas", "Offner", "thomas@offner.at", createAddress("Gasse 23", "Wien", "1020")));
  }

  @Test
  public void testFindByUsername() {
    User user = userRepository.findByUsername("lubos.bistak");
    assertNotNull("Lubos", user.getFirstName());
    assertEquals("Bistak", user.getLastName());

    user = userRepository.findByUsername("thomas.offner");
    assertNotNull("Thomas", user.getFirstName());
    assertEquals("Offner", user.getLastName());
  }

  @Test
  public void testFindAll_WithSorting() {
    Iterator<User> usersIterator = userRepository.findAll(new Sort(new Sort.Order(Sort.Direction.DESC, "lastName"))).iterator();

    assertEquals("Offner", usersIterator.next().getLastName());
    assertEquals("Bistak", usersIterator.next().getLastName());
  }

  @Test
  public void testFindByLastNameOrFirstNameAllIgnoreCase() throws Exception {
    User user = userRepository.findByFirstNameOrLastNameAllIgnoreCase("luBos", null);
    assertEquals("Bistak", user.getLastName());

    user = userRepository.findByFirstNameOrLastNameAllIgnoreCase(null, "offneR");
    assertEquals("Thomas", user.getFirstName());
  }

  @Test
  public void testFindByUserAddressPostalCode() throws Exception {
    User user = userRepository.findByUserAddressPostalCode("85101");
    assertEquals("Bistak", user.getLastName());

    user = userRepository.findByUserAddressPostalCode("1020");
    assertEquals("Offner", user.getLastName());
  }

  @Test
  public void testFindByLastNameEndsWith() throws Exception {
    User user = userRepository.findByLastNameEndsWith("tak").get(0);
    assertEquals("Bistak", user.getLastName());

    user = userRepository.findByLastNameEndsWith("ner").get(0);
    assertEquals("Offner", user.getLastName());
  }

  @Test
  public void testFindByEmailAddress() throws Exception {
    User user = userRepository.findByEmailAddress("lubos@bistak.sk").get(0);
    assertEquals("Bistak", user.getLastName());
  }

  @Test
  public void testFindAllBySpecification() throws Exception {
    List<User> users = userRepository.findAll(livesInSlovakia());
    assertEquals(1, users.size());
    assertEquals("Bistak", users.get(0).getLastName());

    users = userRepository.findAll(livesInAustria());
    assertEquals(1, users.size());
    assertEquals("Offner", users.get(0).getLastName());

    users = userRepository.findAll(where(livesInSlovakia()).or(livesInAustria()), new Sort(new Sort.Order(Sort.Direction.DESC, "lastName")));
    assertEquals(2, users.size());
    assertEquals("Offner", users.get(0).getLastName());
    assertEquals("Bistak", users.get(1).getLastName());
  }

  private User createUser(String userName, String firstName, String lastName, String email, Address address) {
    User user = new User();
    user.setUsername(userName);
    user.setFirstName(firstName);
    user.setLastName(lastName);
    user.seteMail(email);
    user.setUserAddress(address);
    return user;
  }

  private Address createAddress(String street, String city, String postalCode) {
    Address address = new Address();
    address.setStreet(street);
    address.setCity(city);
    address.setPostalCode(postalCode);
    return address;
  }
}
