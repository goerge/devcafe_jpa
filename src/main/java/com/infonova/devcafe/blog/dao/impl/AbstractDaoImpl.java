package com.infonova.devcafe.blog.dao.impl;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.infonova.devcafe.blog.dao.AbstractDao;

public abstract class AbstractDaoImpl<T, K> implements AbstractDao<T, K> {

	protected Class<T> entityClass;

	@SuppressWarnings("unchecked")
	public AbstractDaoImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	@PersistenceContext
	protected EntityManager em;

	@Transactional
	public void saveEntity(T entity) {
		em.persist(entity);
	}

	@Transactional
	public T updateEntity(T entity) {
		return em.merge(entity);
	}

	@Transactional(readOnly = true)
	public T findEntity(K id) {
		return em.find(entityClass, id);
	}

	@Transactional
	public void deleteEntity(T entity) {
		em.remove(em.merge(entity));
	}
}