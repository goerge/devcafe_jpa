package com.infonova.devcafe.blog.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.infonova.devcafe.blog.dao.UserDao;
import com.infonova.devcafe.blog.model.User;
import com.infonova.devcafe.blog.model.UserPreview;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, String> implements UserDao {

	@Transactional(readOnly = true)
	public List<User> findByFirstName(String firstName) {
		return em.createQuery("select u from User u where u.firstName=:firstName", User.class).setParameter("firstName", firstName).getResultList();
	}

	@Transactional(readOnly = true)
	public List<User> findByLastName(String lastName) {
		return em.createNamedQuery(User.FIND_BY_LASTNAME, User.class).setParameter("lastName", lastName).getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public User findByEMail(String eMail) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
		ParameterExpression<String> parameter = criteriaBuilder.parameter(String.class, "eMail");

		Root<User> user = criteriaQuery.from(User.class);
		criteriaQuery.select(user).where(criteriaBuilder.equal(user.get("eMail"), parameter));

		TypedQuery<User> managerQuery = em.createQuery(criteriaQuery);
		managerQuery.setParameter(parameter, eMail);
		return managerQuery.getSingleResult();
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserPreview> listUserPreviews() {
		return em
				.createQuery(
						"SELECT new com.infonova.devcafe.blog.model.UserPreview(CONCAT(u.firstName,' ', u.lastName), CONCAT(a.street, ', ',a.postalCode,' ',a.city)) from User u join u.userAddress a ORDER BY u.id",
						UserPreview.class).getResultList();
	}
}
