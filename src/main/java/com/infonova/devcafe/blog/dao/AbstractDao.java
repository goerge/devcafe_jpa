package com.infonova.devcafe.blog.dao;

public interface AbstractDao<T, K> {

	void saveEntity(T entity);

	void deleteEntity(T entity);

	T updateEntity(T entity);

	T findEntity(K id);

}
