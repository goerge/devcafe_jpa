package com.infonova.devcafe.blog.dao;

import com.infonova.devcafe.blog.model.Blog;

public interface BlogDao extends AbstractDao<Blog, Long> {

}
