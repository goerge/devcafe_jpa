package com.infonova.devcafe.blog.dao.impl;

import org.springframework.stereotype.Repository;

import com.infonova.devcafe.blog.dao.BlogDao;
import com.infonova.devcafe.blog.model.Blog;

@Repository
public class BlogDaoImpl extends AbstractDaoImpl<Blog, Long> implements BlogDao {

}
