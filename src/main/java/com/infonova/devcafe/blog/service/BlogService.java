package com.infonova.devcafe.blog.service;

import com.infonova.devcafe.blog.model.Blog;

public interface BlogService {

	void addBlogEntry(String username, Blog entry);
}
