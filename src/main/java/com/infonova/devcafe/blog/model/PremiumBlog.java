package com.infonova.devcafe.blog.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("PREMIUM")
public class PremiumBlog extends Blog {

	@Column
	private Integer visits;

	@ManyToMany
	@JoinTable(name = "blog_visitors", //
	joinColumns = { @JoinColumn(name = "blog_id", referencedColumnName = "id") }, //
	inverseJoinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "username") })
	private Set<User> visitors;

	public Integer getVisits() {
		return visits;
	}

	public void setVisits(Integer visits) {
		this.visits = visits;
	}

	public Set<User> getVisitors() {
		return visitors;
	}

	public void setVisitors(Set<User> visitors) {
		this.visitors = visitors;
	}
}